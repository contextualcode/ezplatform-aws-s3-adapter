<?php

namespace ContextualCode\EzPlatformAwsS3Adapter\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\Yaml\Yaml;

class EzPlatformAwsS3AdapterExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('commands.yaml');
        $loader->load('services.yaml');
    }

    public function prepend(ContainerBuilder $container): void
    {
        $this->prependExtension($container,'twig');
        $this->prependExtension($container,'ibexa_design_engine');
    }

    protected function prependExtension(
        ContainerBuilder $container,
        string $extension,
        ?string $configFileName = null
    ): void {
        if ($configFileName === null) {
            $configFileName = $extension;
        }

        $configFile = __DIR__ . '/../Resources/config/' . $configFileName . '.yaml';
        $config = Yaml::parseFile($configFile);
        $container->prependExtensionConfig($extension, $config);
        $container->addResource(new FileResource($configFile));
    }
}