<?php

namespace ContextualCode\EzPlatformAwsS3Adapter\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

abstract class Base extends Command
{
    protected static $defaultName;
    protected $description;
    protected $options = [];
    protected $arguments = [];

    /** @var SymfonyStyle */
    protected $io;

    /** @var ProgressBar */
    private $progressBar;

    protected function configure(): void
    {
        $this->setDescription($this->description);

        foreach ($this->options as $name => $info) {
            $default = $info['default'] ?? null;
            $this->addOption($name, null, $info['mode'], $info['description'], $default);
        }

        foreach ($this->arguments as $name => $info) {
            $default = $info['default'] ?? null;
            $this->addArgument($name, $info['mode'], $info['description'], $default);
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io = new SymfonyStyle($input, $output);

        return $this->doRun($input);
    }

    protected function doRun(InputInterface $input): int
    {
        return 0;
    }

    protected function progressStart(int $max): void
    {
        $this->progressBar = $this->io->createProgressBar();
        $this->progressBar->setFormat('very_verbose');
        $this->progressBar->start($max);
    }

    protected function progressAdvance(): void
    {
        $this->progressBar->advance(1);
    }

    public function progressFinish(): void
    {
        $this->progressBar->finish();
        $this->io->newLine(2);
        $this->progressBar = null;
    }
}