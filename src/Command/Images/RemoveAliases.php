<?php

namespace ContextualCode\EzPlatformAwsS3Adapter\Command\Images;

use ContextualCode\EzPlatformAwsS3Adapter\Command\Base;
use DateTime;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\Statement;
use Doctrine\DBAL\ParameterType;
use Error;
use Exception;
use Ibexa\Core\IO\IOConfigProvider;
use Ibexa\Core\IO\IOServiceInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;

class RemoveAliases extends Base
{
    protected static $defaultName = 'ezplatform:images:remove-aliases';
    protected $description = 'Removes generated image aliases';
    protected $options = [
        'created-before' => [
            'mode' => InputOption::VALUE_OPTIONAL,
            'description' => 'Removes only aliases created before specified a time provided in any format understood by DateTime.',
            'default' => '24 hours ago',
        ],
    ];

    /** @var SymfonyStyle */
    protected $io;

    /** @var Connection */
    private $db;

    /** @var IOServiceInterface */
    private $imageIo;

    private $storagePath;
    private $imagesPrefix;

    public function __construct(
        Connection $connection,
        IOServiceInterface $imageIo,
        IOConfigProvider $configResolver,
        string $imagesStoragePrefix
    ) {
        $this->db = $connection;
        $this->imageIo = $imageIo;
        $this->storagePath = $configResolver->getLegacyUrlPrefix();
        $this->imagesPrefix = $imagesStoragePrefix;

        parent::__construct(null);
    }

    protected function doRun(InputInterface $input): int
    {
        $createdBefore = $input->getOption('created-before');
        try {
            $createdBeforeDate = new DateTime($createdBefore);
        } catch(Exception $e) {
            $this->io->error('Unable to parse "' . $createdBefore . '" DateTime fromat');

            return 1;
        }


        $count = (int) $this->getStatementImageAliases($createdBeforeDate, true)->fetchColumn();
        $title = 'Removing ' . $count . ' image aliases generated before ' . $createdBeforeDate->format('c');
        $this->io->title($title);

        $stmt = $this->getStatementImageAliases($createdBeforeDate);
        $this->progressStart($count);
        while ($row = $stmt->fetch()) {
            $fileId = str_replace($this->storagePath . '/' . $this->imagesPrefix . '/', '', $row['name']);

            try {
                $file = $this->imageIo->loadBinaryFile($fileId);
                $this->imageIo->deleteBinaryFile($file);
            } catch(Exception|Error $e) {
                $this->io->error($e->getMessage());
            }

            $this->progressAdvance();
        }
        $this->progressFinish();

        return 0;
    }

    protected function getStatementImageAliases(
        DateTime $createdBefore,
        bool $count = false
    ): Statement {
        $aliasesPath = $this->storagePath . '/' . $this->imagesPrefix . '/_aliases/%';

        $q = $this->db->createQueryBuilder();
        $q->select($count ? 'COUNT(f.name_hash)' : 'f.name')
            ->from('ezdfsfile', 'f')
            ->where(
                $q->expr()->lt('f.mtime', ':created_before')
            )
            ->andWhere(
                $q->expr()->like('f.name', ':aliases_path')
            )
            //->andWhere(FILTER BY PATH)
            ->setParameter('aliases_path', $aliasesPath, ParameterType::STRING)
            ->setParameter('created_before', $createdBefore->getTimestamp(), ParameterType::INTEGER);

        return $q->execute();
    }
}