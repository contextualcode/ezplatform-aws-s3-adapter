<?php

declare(strict_types=1);

namespace ContextualCode\EzPlatformAwsS3Adapter\Service;

use Ibexa\Contracts\Core\Repository\ContentService;
use Ibexa\Contracts\Core\Repository\LocationService;
use Ibexa\Contracts\Core\SiteAccess\ConfigResolverInterface;
use Ibexa\Contracts\Core\Repository\ContentTypeService;
use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Ibexa\Core\FieldType\Image\Value as ImageValue;
use Ibexa\Core\Base\Exceptions\InvalidArgumentException;
use Ibexa\Core\FieldType\ImageAsset\AssetMapper as Base;
use Ibexa\Core\IO\IOConfigProvider;

class AssetMapper extends Base
{
    protected IOConfigProvider $iOConfigProvider;
    protected string $awsS3BaseUrl;

    public function __construct(
        ContentService $contentService,
        LocationService $locationService,
        ContentTypeService $contentTypeService,
        ConfigResolverInterface $configResolver,
        IOConfigProvider $iOConfigProvider,
        string $awsS3BaseUrl
    ) {
        parent::__construct($contentService, $locationService, $contentTypeService, $configResolver);
        $this->iOConfigProvider = $iOConfigProvider;
        $this->awsS3BaseUrl = $awsS3BaseUrl;
    }

    public function getAwsS3Url(string $path): string
    {
        $storageDir = '/' . ltrim($this->iOConfigProvider->getLegacyUrlPrefix(), '/');
        if (strpos($path, $storageDir) === 0) {
            $path = mb_substr($path, strlen($storageDir));
        }

        return $this->awsS3BaseUrl . $path;
    }

    /**
     * Returns field value of the Image Asset from specified content.
     *
     * @param \eZ\Publish\API\Repository\Values\Content\Content $content
     *
     * @return \eZ\Publish\Core\FieldType\Image\Value
     *
     * @throws \eZ\Publish\API\Repository\Exceptions\InvalidArgumentException
     * @throws \eZ\Publish\API\Repository\Exceptions\NotFoundException
     */
    public function getAssetValue(Content $content): ImageValue
    {
        if (!$this->isAsset($content)) {
            throw new InvalidArgumentException('contentId', "Content {$content->id} is not an image asset.");
        }

        $value = $content->getFieldValue($this->getContentFieldIdentifier());
        $path = $value->uri;
        $value->uri = strpos($path, '/') === 0 ? $this->getAwsS3Url($path) : $path;
        return $value;
    }
}
